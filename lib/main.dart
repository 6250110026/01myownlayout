import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      title: 'rental house',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),

      home: const MyHomePage(title: 'Rental House'),
    );


  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final myController_username = TextEditingController();//Create a TextEditingController
  final myController_password = TextEditingController();
  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController_username.dispose();
    myController_password.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.insert_emoticon),
        title: Text(widget.title),
        actions: [IconButton(onPressed: (){}, icon: Icon(Icons.add_to_queue_rounded))],
      ),
      body: Container(
        color: Colors.lightGreenAccent[100],
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Icon(
                Icons.icecream_rounded,
                size: 180,color: Colors.white
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Row child 1',
                    style: TextStyle(
                      fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.blueGrey

                    ),
                  ),
                  Text(
                    'Row child 1',
                    style: TextStyle(
                      fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.blueGrey
                    ),
                  ),
                  Text(
                    'Row child 1',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                        color: Colors.blueGrey
                    ),
                  ),
                ],
              ),

              Padding(

                padding: const EdgeInsets.all(30),
                child: Text(
                  'ลงทะเบียน',
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold,color: Colors.green),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  controller: myController_username,
                  decoration: InputDecoration(
                    hintText: 'ชื่อ',
                    fillColor: Colors.white54,
                    filled: true,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(100)),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right:  15, bottom: 30),
                child: TextField(
                  controller: myController_password,
                  decoration: InputDecoration(
                    hintText: 'รหัส',
                    fillColor: Colors.white54,
                    filled: true,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(100)),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary:  Colors.blueGrey,//สี foreground
                      onPrimary: Colors.white,//สี เมื่อคลิก
                      padding:
                      EdgeInsets.symmetric(horizontal: 30,vertical: 15),
                      textStyle: TextStyle(fontSize: 20),

                    ),
                    onPressed: (){
                      myController_username.clear();// ปุ่มเครียดค่า
                      myController_password.clear();// ปุ่มเครียดค่า
                    },
                    child: Text('Cacel'),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary:  Colors.blueGrey,//สี foreground
                      onPrimary: Colors.white,//สี เมื่อคลิก
                      padding:
                      EdgeInsets.symmetric(horizontal: 30,vertical: 15),
                      textStyle: TextStyle(fontSize: 20),

                    ),
                    onPressed: () => displayToast(),//print('Hell Login!!!'),
                    child: Text('Login'),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }// end build

  void  displayToast(){
    String username = myController_username.text;
    String password = myController_password.text;
    Fluttertoast.showToast(
      msg: 'Your username: $username  \n Your username: $password',
      toastLength: Toast.LENGTH_SHORT,

    );
  }

}// end class _myHomepage
